
#include<iostream>
#include<queue>

using namespace std;

class Node
{
public:
	int x,y;
	char data;

	Node(int x_c, int y_c, char data_c)
	{x = x_c; y=y_c; data = data_c;}
};

void Print_Stack(queue<Node> &q)
{
	while( !q.empty() )
	{
		Node top = q.front();
		q.pop();
		cout << top.x << ' ' << top.y << ' ' << top.data << '\n';
	}
	cout << " Size is " << q.size() << '\n';
}


void Fill_Stack_using_DFS(queue<Node> &q, char arr[4][4], bool filled[4][4])
{

	char t = arr[0][0];
	q.push(0,0, t);		//Push start of the grid
	
	int min_time = 0;	//Minimum time intially = 0
	int first_counter = 0;	//if first_counter==0 then min_time = 0
	int time_count = 0;

	while( !q.empty() )
	{
		
		Node top = q.front();
		
		if(!filled[top.x][top.y])		//Check if already visited
		{
			if(top.data=='B')		//Check if it is the destination
			{
				if(first_counter==0)	//First time reaching the destination
				{
					min_time = time_count;
					first_counter = 1;	//Change first counter just once
					continue;
				}

				else			//first counter already changed
				{
					if( min_time > time_count)
					{
						min_time = time_count;
					}
				}
			}
		}
		q.pop();


		//Visit all the neighbouring nodes do some process
		if(!filled[top.x][top.y])
		{
			filled[top.x][top.y] = true;

			q.push(top.x,top.y+1, arr[top.x][top.y+1]);
			q.push(top.x,top.y-1, arr[top.x][top.y-1]);
			q.push(top.x-1,top.y, arr[top.x-1][top.y]);
			q.push(top.x+1,top.y, arr[top.x+1][top.y]);
		}
	}

	time_count++;
}


int main(void)
{
	queue<Node> q;
	
	//Node n(0,0,0);

	int arr1[4][4] = {{0,1,2,3}, {4,5,6,7}, {8,9,10,11}, {12,13,14,15}};

	bool fill[4][4];

	for(int if1 = 0; if1 < 4; if1++)
	{
		for(int jf1 = 0; jf1 < 4; jf1++)
		{
			fill[if1][jf1] = false;
		}
	}

	q.push(Node(0,0,'.'));
	q.push(Node(0,1,'.'));
	q.push(Node(0,2,'.'));

	Print_Stack(q);

	return 0;
}

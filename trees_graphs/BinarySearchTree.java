
import java.lang.Math;
import java.util.*;

/*
Java implementation of BinarySearchTree.
C++ implementation is at ~/Documents/oldPractice/Home_Windows/Solutions/codingPractice/
*/

public class BinarySearchTree{
	

	public static void main(String[] args){

		BinarySearchTree bst = new BinarySearchTree();

		bst.start();
	}

	protected void start(){

		this.root = null;

/*
		int []arr = {-1, 1, 2, 4, 5};
		root = this.arrayToTree(arr, 0, 4);
*/
		this.insertValue(4);
		this.insertValue(5);
		this.insertValue(2);
		this.insertValue(1);
		this.insertValue(-1);

		this.preOrderTraversal(root);
		this.postOrderTraversal(root);
		this.inOrderTraversal(root);
		System.out.println(this.getHeight(root));
		System.out.println(this.checkBalance(root));

		List<List<Node>> allLists = new ArrayList<List<Node>>();
		List<Node> baseList = new ArrayList<Node>();
		baseList.add(root);

		createLinkedListForNewLevel(baseList, allLists);

	}

	protected void insertValue(int val){

		root = this.addItem(root, val);
		numberOfNodes++;
	}

	protected void preOrderTraversal(Node node){

		if(node != null){

			node.printNode();
			preOrderTraversal(node.left);
			preOrderTraversal(node.right);
		}
	}

	protected void postOrderTraversal(Node node){

		if(node != null){

			preOrderTraversal(node.left);
			preOrderTraversal(node.right);
			node.printNode();
		}
	}

	protected void inOrderTraversal(Node node){

		if(node != null){

			preOrderTraversal(node.left);
			node.printNode();
			preOrderTraversal(node.right);
		}
	}


	//Each recursive call returns the node with which it was called
	protected Node addItem(Node node, int itemVal){

		if(node == null){

			return new Node(itemVal);

		}
		else if(node.val > itemVal){

			node.left = addItem(node.left, itemVal);
		}
		else{

			node.right = addItem(node.right, itemVal);
		}

		return node;
	}

	protected int getHeight(Node node){

		if(node == null){

			return 0;
		}

		else{

			return Math.max(this.getHeight(node.left), this.getHeight(node.right))
					+ 1;
		}
	}

	protected boolean checkBalance(Node node){

		if(node == null){

			return true;
		}
		else{

			return Math.abs(this.getHeight(node.left) - this.getHeight(node.right)) < 2;

		}
	}

	protected Node arrayToTree(int []arr, int start, int end){

		Node node;
		if(start > end){

			node = null;
		}
		else{

			int mid = start + end;
			node = new Node(arr[mid]);

			node.left = arrayToTree(arr, start, mid - 1);
			node.right = arrayToTree(arr, mid + 1, end);
		}

		return node;
	}

	protected void createLinkedListForNewLevel(List<Node> previousLevelList, List<List<Node>> allLists){

		List<Node> currentLevelList = new ArrayList<Node>();

		if(!previousLevelList.isEmpty()){

			for(Node node : previousLevelList){

				if(node.left != null){

					node.left.printNode();
					currentLevelList.add(node.left);
				}
				if(node.right != null){

					node.right.printNode();
					currentLevelList.add(node.right);
				}
			}

			allLists.add(currentLevelList);
			createLinkedListForNewLevel(currentLevelList, allLists);
		}
	}

	protected Node getRandomNode() {

		Random random = new Random();

		int randomNumber = random.nextInt(numberOfNodes);
		System.out.println(" random no. " + randomNumber);

		return getNodeForNumberOfMoves(root, randomNumber);
	}

	protected Node getNodeForNumberOfMoves(Node node, int randomNumber) {

		Node targetNode = null;

		if(node == null) {

			return null;
		}

		if(randomNumber == 0) {

			return node;
		}

		targetNode = getNodeForNumberOfMoves(node.left, randomNumber - 1);

		if(targetNode != null) {

			return targetNode;
		}

		return getNodeForNumberOfMoves(node.right, randomNumber - 1);
	}

	protected static Node root;
	protected int numberOfNodes = 0;
}

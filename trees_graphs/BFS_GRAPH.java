
import java.util.Queue;
import java.util.LinkedList;

public class BFS_GRAPH{
	

	public static void main(String []args){

		BFS_GRAPH bfs_g = new BFS_GRAPH();

		bfs_g.start();
	}

	protected void start(){

		// this.basicBFS(0);
		this.WGB_BFS(0);
	}

	protected void basicBFS(int vertexNumber){

		Queue q = new LinkedList<Integer>();
		seen[vertexNumber] = true;

		q.add(vertexNumber);

		while(!q.isEmpty()){

			int currentVertex = (Integer)q.remove();
			System.out.println(" current: " + currentVertex);

			for(int v=0; v < seen.length; v++){

				if( (v != currentVertex)
					&& !seen[v] 
					&& adjacencyMatrix[currentVertex][v]){

					seen[v] = true;

					System.out.println(" add: " + v);

					q.add(v);
				}
			}
		}
	}

	/*
	HERE all the vertices outside of the Q are white
	All the vertices inside the Q are grey
	All the vertices processed and removed from the Q are black
	*/
	protected void WGB_BFS(int vertexNumber){

		Queue q = new LinkedList<Integer>();

		//Grey
		seen[vertexNumber] = true;
		//color[vertexNumber] = GREY;  (Source Vertex)

		q.add(vertexNumber);
		
		while(!q.isEmpty()){

			//Vertex in front of queue is the vertex closest to q if edges are constant
			int currentVertex = (Integer)q.remove();
			System.out.println(" current: " + currentVertex);

			for(int v=0; v < seen.length; v++){

				if( (v != currentVertex) //Don't check for color[v] here since we are allowed to relax an already seen vertex
					&& adjacencyMatrix[currentVertex][v]){

					seen[v] = true;

					//color[v] = GREY;

					if(distanceToVertex[v] > 1 + distanceToVertex[currentVertex]){

						distanceToVertex[v] = 1 + distanceToVertex[currentVertex];
						System.out.println(" relaxed d[v]: " + distanceToVertex[v]);

						System.out.println(" add: " + v);
						q.add(v);
					}
				}
			}

			//color[v] = BLACK;
		}
	}

	protected	boolean [][]adjacencyMatrix = {{true, true, true, true, false},
									          {true, true, false, false, true},
									          {true, false, true, true, true},
									          {true, false, true, true, true},
									          {false, true, true, true, true}};

	protected	boolean seen[] = {false, false, false, false, false};
	protected	int distanceToVertex[] = {0, 12, 12, 12, 12};
	protected	int parentVertex[] = {-1, -1, -1, -1, -1};
	protected int dfsNum = 0;
}

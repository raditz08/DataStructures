
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.LinkedList;

public class DijkstraOnGraph{
	

	public static void main(String []args){

		DijkstraOnGraph bfs_g = new DijkstraOnGraph();

		bfs_g.start();
	}

	protected void start(){

		// this.dijkstraPriorityQueue(0);
		this.dijkstraDistanceArray(0);
	}

	protected void dijkstraDistanceArray(int sourceVertex){

		boolean uSeenAlready = false;
		int currentVertex = -1;

		while(!uSeenAlready){

			currentVertex = this.findNotSeenVertexWithMinDistance();

			if(currentVertex != -1){

				this.relaxNeighbours(currentVertex);
				seen[currentVertex] = true;
			}
			else{
				uSeenAlready = true;
			}
		}

		System.out.println(" d[4]: " + distanceToVertex[4]); //should be 40
	}

	protected int findNotSeenVertexWithMinDistance(){

		int minDistance = Integer.MAX_VALUE;
		int vertexWithMinDistance = -1;

		for(int vertex=0; vertex < seen.length; vertex++){

			if( !seen[vertex] && minDistance > distanceToVertex[vertex]){

				minDistance = distanceToVertex[vertex];
				vertexWithMinDistance = vertex;
			}
		}

		return vertexWithMinDistance;
	}

	protected void relaxNeighbours(int currentVertex){

		int currVerDist = distanceToVertex[currentVertex];

		for(int vertex=0; vertex < seen.length; vertex++){
			
			if(adjacencyMatrix[currentVertex][vertex] != -1){
				distanceToVertex[vertex] = Math.min(distanceToVertex[vertex], currVerDist + adjacencyMatrix[currentVertex][vertex]);
			}
		}
	}

	/*
		O( n*Removal cost + m*Insertion cost + Traversing over all edges)

		O(n^2 + (m+n)Log(n)) - Adjacency matrix. nLog(n) for removal and m(Log(n)) for insertion in priorityQ, n^2 for finding neighbours

		O((m+n)Log(n))       - Adjacency list. nLog(n) for removal and m(Log(n)) for insertion in priorityQ

		C++ - priorityQ

		// Using lambda to compare elements.
    	auto cmp = [](int left, int right) { return (left ^ 1) < (right ^ 1);};
    	std::priority_queue<int, std::vector<int>, decltype(cmp)> q3(cmp);
	*/
	protected void dijkstraPriorityQueue(int sourceVertex){

		CustomComparator custom = new CustomComparator();
		PriorityQueue<Integer> q = new PriorityQueue<Integer>(custom);

		seen[sourceVertex] = true;

		q.add(sourceVertex);
		
		while(!q.isEmpty()){ //O(n) since each vertex only once

			//Vertex in front of queue is the vertex closest to q if edges are constant
			int currentVertex = (Integer)q.remove(); //O(n*log(n)) since each vertex only once
			System.out.println(" current: " + currentVertex);

			for(int v=0; v < seen.length; v++){ //O(n)

				if( (v != currentVertex)
					&& (adjacencyMatrix[currentVertex][v] != -1)){

					if(distanceToVertex[v] > adjacencyMatrix[currentVertex][v] + distanceToVertex[currentVertex]){

						distanceToVertex[v] = adjacencyMatrix[currentVertex][v] + distanceToVertex[currentVertex];
						System.out.println(" relaxed d[v]: " + distanceToVertex[v]);

						System.out.println(" add: " + v);
						if(q.contains(v)){ System.out.println(" remove v: " + v); q. remove(v);}
						q.add(v); //O(m)
					}
					else{
						System.out.println(" v: " + v + " distanceToVertex[v]: " + distanceToVertex[v]);
					}					
				}
			}
		}
	}

	protected	int [][]adjacencyMatrix = {{0, 20, 30, 40, -1},
									       {20, 0, -1, -1, 40},
									       {30, -1, 0, 10, 10},
									       {40, -1, 10, 0, 10},
									       {-1, 40, 10, 10, 0}};

	protected	boolean seen[] = {false, false, false, false, false};
	protected	int distanceToVertex[] = {0, 120, 120, 120, 120};
	protected	int parentVertex[] = {-1, -1, -1, -1, -1};

	protected class CustomComparator implements Comparator<Integer>{

		@Override
		public int compare(Integer left, Integer right){

			return distanceToVertex[left] - distanceToVertex[right];
		}
	}
}

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        
        vector<int> res(k);
            
        unordered_map<int, int> um;
        
        for(auto num : nums) {
            
            um[num]++;
        }
/*
        PRIORITY Q + UNORDERED MAP SOLUTION 

        priority_queue<pair<int, int>> q;
                
        for(auto it = um.begin(); it != um.end(); it++) {
                        
            q.push(make_pair<>(it->second, it->first));
        }
                
        for(int index = 0; index < k; index++) {
                    
                     
            int top = q.top().second;
            q.pop();
            res.push_back(top);
        }*/


        
        for(int index = 0; index < k; index++) {
            
            int maxFreq = INT_MIN;
            int num;
            
            
            for(auto it = um.begin(); it != um.end(); it++) {
                
                if(maxFreq < it -> second) {
                    
                    maxFreq = it->second;
                    num = it->first;
                }
            }
            
            res[index] = num;
            
            um.erase(num);
        }
        
        return res;
    }
};

public class DFS_GRAPH{
	

	public static void main(String []args){

		DFS_GRAPH dfs_g = new DFS_GRAPH();

		dfs_g.start();
	}

	protected void start(){


		this.basicDFS(0);
		this.WGB_DFS(0);
	}

	protected void basicDFS(int vertexNumber){

		seen[vertexNumber] = true;

		System.out.println(" " + vertexNumber);

		for(int v=0; v < seen.length; v++){

			if( (v != vertexNumber)
				&& !seen[v] 
				&& adjacencyMatrix[vertexNumber][v]){

				basicDFS(v);
			}
		}
	}

	protected void WGB_DFS(int vertexNumber){

		System.out.println(
			" current: " + vertexNumber + 
			" dfsNum: " + dfsNum);

		//Shade current first to grey
		colour[vertexNumber] = 1;
		dfsNumber[vertexNumber] = dfsNum++; //The order number vertexNumber in the current ordering

		for(int v=0; v < seen.length; v++){

			if( (v != vertexNumber)
				&& (adjacencyMatrix[vertexNumber][v])){

				if(colour[v] == 0){	//Grey-White Forward edge

					WGB_DFS(v);
				}
				else if(colour[v] == 1){	//Grey-Grey Back edge

					System.out.println(" back to " + v);
				}
				else if(colour[v] == 2){	//Grey-Black cross edge to another branch(Doesn't exist in undirected graph) 

					System.out.println(" cross to " + v);
				}
			}
		}
	}

	protected	boolean [][]adjacencyMatrix = {{true, true, true, true, false},
									          {true, true, false, false, true},
									          {true, false, true, true, true},
									          {true, false, true, true, true},
									          {false, true, true, true, true}};

	protected	boolean seen[] = {false, false, false, false, false};
	protected	int colour[] = {0, 0, 0, 0, 0};
	protected	int dfsNumber[] = {0, 0, 0, 0, 0};
	protected int dfsNum = 0;
}


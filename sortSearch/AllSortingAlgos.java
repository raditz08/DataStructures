
import java.util.*;

class AllSortingAlgos {
	

	public static void main(String []args) {

		AllSortingAlgos asa = new AllSortingAlgos();

		// int []arr = {5, 2, 1, 7, 9};
		int []arr = {5, 9, 7, 5, 2, 8, 7};

		// asa.bubbleSort(arr);
		// asa.insertionSort(arr);
		// asa.mergeSort(arr);
		asa.quickSort(arr);
		// asa.selectionSort(arr);
		System.out.println(Arrays.toString(arr));
	}

	//O(n^2)
	protected void bubbleSort(int []arr) {

		for(int pass = 1; pass < arr.length; pass++) {

			for(int index = 0; index < arr.length-(pass); index++) {

				if(arr[index] > arr[index+1]) { //Keep swapping till higher is at the end

					int tmp = arr[index];
					arr[index] = arr[index+1];
					arr[index+1] = tmp;
				}
			}
		}
	}

	//O(m+n) where m is the no. of out-of-order elements
	//Makes use of pre-sorting
	//Use when array is kind of pre-sort
	protected void insertionSort(int []arr) {

		for(int sortedLength = 1; sortedLength < arr.length; sortedLength++) {

			int index = sortedLength; //The index for the element to be inserted into the sorted array

			//Insert the next ele(sortedLength) into already sorted array
			while(index > 0 && arr[index] < arr[index-1]) {

				int tmp = arr[index];
				arr[index] = arr[index-1];
				arr[index-1] = tmp;

				index--;
			}
		}
	}

	//O(n^2) worst, O(nlog(n)) - Average
	protected void quickSort(int []arr) {

		partition(arr, 0, arr.length - 1);
	}

	protected void partition(int []arr, int left, int right) {

		if(right - left < 2) {

			return;
		}

		//pivot index itself doesn't matter since that element might get swapped
		int pivot = (left+right)/2;
		int pivotValue = arr[pivot];

		int leftIndex = left,
			rightIndex = right;

		//Swap all left-greater than ele. with right-less ele.
		while(leftIndex < rightIndex) {

			//After the 1st swap this will be true. So will do leftIndex++
			while(arr[leftIndex] < pivotValue)
				leftIndex++;
			
			while(arr[rightIndex] > pivotValue)
				rightIndex--;

			if(leftIndex < rightIndex) {

				int tmp = arr[leftIndex];
				arr[leftIndex] = arr[rightIndex];
				arr[rightIndex] = tmp;

				leftIndex++;
				rightIndex--;
			}
		}

		//Partition individual left and right sides
		partition(arr, left, pivot -1);
		partition(arr, pivot, right);
	}

	protected void selectionSort(int []arr) {

		for(int index = 0; index < arr.length - 1; index++) {

			int minValue = arr[index];
			int minIndex = index;

			for(int searchIndex = index + 1; searchIndex < arr.length; searchIndex++) {

				if(arr[searchIndex] < minValue) {

					minValue = arr[searchIndex];
					minIndex = searchIndex;
				}
			}

			//swap
			int tmp = arr[index];
			arr[index] = arr[minIndex];
			arr[minIndex] = tmp;
		}
	}

	protected void mergeSort(int []arr) {

		int []tmp = new int[arr.length];

		for(int mergeLength = 1; mergeLength < arr.length; mergeLength *= 2) {

			for(int index = 0; index + mergeLength < arr.length; index += 2*mergeLength) {

				merge(arr, tmp, index, index + mergeLength-2, Math.min(index + 2*mergeLength - 1, arr.length-index-1));
			}
		}

		// System.out.println(Arrays.toString(arr));
	}

	protected void merge(int []arr, int []tmp, int leftStart, int leftEnd, int rightEnd) {

		System.out.println(Arrays.toString(arr));
		System.out.println(Arrays.toString(tmp));

		int leftIndex = leftStart,
			tmpIndex = leftStart,
			rightIndex = leftEnd + 1,
			num = rightEnd - leftStart + 1;

		//Merge 2 sub-arrays from arr into tmp
		while(leftIndex <= leftEnd && rightIndex <= rightEnd) {

			if(arr[leftIndex] < arr[rightIndex]) {

				tmp[tmpIndex++] = arr[leftIndex++];
			}
			else {
				tmp[tmpIndex++] = arr[rightIndex++];
			}
		}

		while(leftIndex <=leftEnd) {

			tmp[tmpIndex++] = arr[leftIndex++];
		}
		while(rightIndex <=rightEnd) {

			tmp[tmpIndex++] = arr[rightIndex++];
		}

		//Copy these back into arr
		for(int i = leftStart; i <= rightEnd; i++) {

			arr[i] = tmp[i];
		}
	}
}



import java.util.*;

class MergeSortedArrays {
	

	public static void main(String []args) {

		MergeSortedArrays asa = new MergeSortedArrays();

		int []arr1 = {5, 7, 8, 9, 12, 14, 18, 0, 0, 0, 0};
		int []arr2 = {4, 8, 9, 11};
		asa.mergeTwoArrays(arr1, arr2, 7, 4);

		System.out.println(Arrays.toString(arr1));
	}

	protected void mergeTwoArrays(int []arr1, int []arr2, int m, int n) {

		int finalEnd = m + n - 1;

		int aEnd = m - 1,
			bEnd = n - 1;

		while(bEnd >= 0) {

			if(aEnd >= 0 && arr1[aEnd] > arr2[bEnd]) {

				arr1[finalEnd] = arr1[aEnd];
				aEnd--;
			}
			else {

				arr1[finalEnd] = arr2[bEnd];
				bEnd--;
			}

			finalEnd--;
		}



/*		INSERTION SORT

		for(int index2 = 0; index2 < arr2.length; index2++) {

			int endIndex = m + index2;
			arr1[endIndex] = arr2[index2];

			while(endIndex > 0 && arr1[endIndex] < arr1[endIndex-1]) {

				int tmp = arr1[endIndex];
				arr1[endIndex] = arr1[endIndex-1];
				arr1[endIndex - 1] = tmp;

				endIndex --;
			}
		}*/
	}
}

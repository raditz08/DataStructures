
import java.util.*;

class DecimalToBinary {
	

	public static void main(String []args) {

		DecimalToBinary asa = new DecimalToBinary();

		double num = 0.72;

		int []arr = new int[32];

		asa.decToBinary(num, arr);

		System.out.println(Arrays.toString(arr));
	}

	protected void decToBinary(double num, int []arr) {

		int sigBit = 0;
		double twoPower = Math.pow((double) 2, (double)(-sigBit));
		boolean finish = false;

		while(!finish) {

			System.out.println(" sigBit: " + sigBit + " twoPower: " + twoPower + " num: " + num);

			if(num < twoPower) {

				sigBit++;
				twoPower = Math.pow((double) 2, (double)(-sigBit));
			}
			else {
				arr[sigBit] = 1;
				num -= twoPower;
			}
			
			if(sigBit >= arr.length || num == 0) {

				System.out.println(" finish true");

				finish = true;
			}
		}
	}
}

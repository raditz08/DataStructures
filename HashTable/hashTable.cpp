
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>

#define HASH_TAB 512	//No of keys

struct hashrec
{
	char *key;
};
class hashtable
{
public:
	hashtable();
	~hashtable();
	//hashrec array1;
unsigned int h2(char *k);		//2 values which are added to find hash value of a given char*
unsigned int h1(char *k);
unsigned int hash(char *k,int i);
unsigned int locate(char *k);
int member(char *k);
void init_hashtable();
int insert(char *k);
char *cpy(char *source);

private:
	hashrec hashtab[HASH_TAB];		//Main data structure. Each cell contains a char *key
};

hashtable::hashtable()
{
	unsigned int b;
	for(b=0;b<HASH_TAB;b++)
		hashtab[b].key=NULL;
}

hashtable::~hashtable()
{
}

unsigned int hashtable::h1(char *k)
{
	unsigned int h;
	unsigned char *c;

	h=0;
	for(c=(unsigned char *)k;*c;c++)		
	{
		h=h*37+ *c;						//VERY IMPORTANT CONVERSION 
		
	return h;						// Iterate through each char's value (unsigned char) and add it to hash value
	}									//Unsigned so that integer value (0-255) is non-negative for character
	
}

unsigned int hashtable::h2(char *k)
{
	unsigned int h;
	unsigned char *c;

	h=0;
	for(c=(unsigned char *)k;*c;c++)
	{
		h=h*31+ *c;						//Find some random no. (key) for each string					
		return h%2==0 ? h+1:h;
	}

}

unsigned int hashtable::hash(char *k,int i)
{
	int o= h1(k)+i*h2(k)%HASH_TAB;
	return h1(k)+i*h2(k)%HASH_TAB;		//Will return some no. within hashtab 
}

unsigned int hashtable::locate(char *k)			// output is empty bucket or end or matching bucket
{
	unsigned int i, b;		//i=buckets probed, b=current bucket address

	for(i=0;i<HASH_TAB;i++)		//Go through the whole hash table
	{
		b=hash(k,i);			//Find the key for k for the given try as hash value is found out by no. of buckets already filled
		if((NULL==hashtab[b].key)||(strcmp(hashtab[b].key,k))==0)		//Empty bucket, 2 keys are equal
			break;
	}
	return b;
};

int hashtable::member(char *k)		//Return 
{
	unsigned int b=locate(k);

	if(NULL==hashtab[b].key) 
	{
		return 0;		//If empty bucket then return 0
	}

	else
	{	
		return (strcmp(hashtab[b].key,k)==0);	//i.e. return 1
	}
}

char *hashtable::cpy(char *source)
{
	char *destination, *d;
	d = destination = (char *)malloc(strlen(source) *sizeof(char) + 1);

	if(d)		//malloc succesfull
		while (*d++=*source++);
	return destination;
}

int hashtable::insert(char *k)		//Insert key here
{
	unsigned int b=locate(k);		//First find if key is located

	if(NULL==hashtab[b].key)		//If empty bucket then insert key
	{
		hashtab[b].key =cpy(k);
		return 1;
	}
			
	else return strcmp(hashtab[b].key,k)==0;	//if strcmp is successfull(0) then insert is also successfull
	}

void hashtable::init_hashtable()
{
	unsigned int b;
	for(b=0;b<HASH_TAB;b++)
		hashtab[b].key=NULL;
}

int main()
{

	char *strings[]={"go","h","i","j","k","h"}	;

 
	hashtable tableClass;
	size_t i;		//String array
	size_t n = sizeof(strings)/sizeof(char*);

	tableClass.init_hashtable();

	for(i=0;i<n;i++)
	{
		if(!tableClass.member(strings[i]))		//0 - return means member not there. So !0 condition means member
		{
			tableClass.insert(strings[i]);		
			
			getchar();
		}

		else
		{
			getchar();
			break;
		}
	}

	return 0;
}
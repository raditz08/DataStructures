/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        
        ListNode curr1 = l1,
                 curr2 = l2,
                 sumCurr = null;
                 
        int sum = 0, quotient = 0, remainder = 0;
                 
        while(curr1 != null || curr2 != null) {
            
            int firstDigit = curr1 != null ? curr1.val : 0;
            int secondDigit = curr2 != null ? curr2.val : 0;
            
            
            sum = firstDigit + secondDigit;
            
            remainder = (sum + quotient)%10;
            quotient = (sum + quotient)/10;
            
            sumCurr = this.addNode(sumCurr,remainder);
            
            if(curr1 != null) curr1 = curr1.next;
            if(curr2 != null) curr2 = curr2.next;
        }
        
        if(curr1 == null && curr2 == null && quotient != 0) {
            
            sumCurr = this.addNode(sumCurr, quotient);
        }
        
        return sumCurr;
    }
    
    protected ListNode addNode(ListNode sumCurr, int remainder) {
        
        if(sumCurr == null) {
            
            sumCurr = new ListNode(remainder);
        }
        else {
            
            ListNode curr = sumCurr;
            
            while(curr.next != null) {
                
                curr = curr.next;
            }
            curr.next = new ListNode(remainder);
        }
        
        return sumCurr;
    }
}
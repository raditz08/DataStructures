
public class LinkedListBasic{
	

	public static void main(String []args) {

		// Node start = new Node(4);

		LinkedListBasic llb = new LinkedListBasic();

		llb.initList();
		// llb.printList();

		Node ktoLast = llb.findKthToLast(3);
		// ktoLast.printNode();

		llb.printList();

		llb.removeDuplicates();
		llb.printList();

		llb.partitionAroundValue(7);
		llb.printList();
		llb.start.printNode();
	}

	protected Node start;

	protected void reverseList() {

		Node curr = start, prev = null, next;
        
        while(curr.next != null) {
            
            next = curr.next;
            
            curr.next = prev;
            prev = curr;
            curr = next;
        }
	}

	protected void partitionAroundValue(int val) {

		System.out.println(" partitionAroundValue: " + val);

		if(start == null) {

			return;
		}

		Node curr = start,
			 head = start;

		while(curr.next != null) {

			if(curr.next.val < val) {

				Node moveToStart = curr.next; //target node saved
				

				moveToStart.next = start;
				start = moveToStart;
				curr.next = curr.next.next; //curr.next moved
			}
			else {

				curr = curr.next;
			}
		}
	}

	protected void removeDuplicates() {

		if(start == null) {

			return;
		}

		Node currNode = start;

		while(currNode != null) {

			Node checkNode = currNode;

			while(checkNode.next != null) {

				if(currNode.val == checkNode.next.val) {

					checkNode.next = checkNode.next.next;
				}
				else {

					checkNode = checkNode.next;
				}
			}
			currNode = currNode.next;
		}
	}


	protected Node findKthToLast(int k) {

		Node forwardPtr = start;
		Node currPtr = start;

		boolean reachedListEnd = false;

		while(k > 0 && !reachedListEnd) {

			if(forwardPtr.next != null) {

				forwardPtr = forwardPtr.next;
				k--;
			}
			else {

				reachedListEnd = true;
			}
		}

		if(reachedListEnd) {

			return null;
		}

		while(forwardPtr.next != null) {

			forwardPtr = forwardPtr.next;
			currPtr = currPtr.next;
		}

		return currPtr;
	}

	protected void printList() {

		System.out.println(" printList ");

		Node curr = start;

		while(curr != null) {

			curr.printNode();
			curr = curr.next;
		}
	}

	protected void initList() {

		 start = addNode(start, 10);
		 addNode(start, 2);
		 addNode(start, 5);
		 addNode(start, 6);
		 addNode(start, 3);
		 addNode(start, 4);
		 addNode(start, 7);
		 addNode(start, 1);
		 addNode(start, 9);
	}

	protected Node addNode(Node head, int val) {

		if(head == null) {
			head = new Node(val);

			return head;
		}

			Node curr = head;
			while(curr.next != null) {

				curr = curr.next;
			}

			curr.next = new Node(val);

		return curr;
	}
}

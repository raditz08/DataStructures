/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode detectCycle(ListNode head) {
        
        if(head == null) {
            
            return null;
        }
        
        ListNode slow = head,
                 fast = head;
                       
        boolean foundCommon = false;
                       
        while(fast != null && fast.next != null && !foundCommon) {
            
            slow = slow.next;
            fast = fast.next.next;
            
            if(slow == fast) {
                
                foundCommon = true;
            }
        }
        
        if(!foundCommon) {
            
            return null;
        }
        
        slow = head;
        
        while(slow != fast) {
            
            slow = slow.next;
            fast = fast.next;
        }
        
        return slow;
    }
}
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        
        ListNode curr = headA;
        
        Set<Integer> s = new HashSet<Integer>();
        
        while(curr != null) {
            
            s.add(curr.val);
            curr = curr.next;
        }
        
        curr = headB;
        
        while(curr != null) {
            
            if(s.contains(curr.val)) {
                
                return curr;
            }
            
            curr = curr.next;
        }
        
        return null;
    }
}

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        
        ListNode currA = headA,
                 currB = headB;
                 
        int lengthA = getListLength(currA);
        int lengthB = getListLength(currB);
        
        currA = headA;
        currB = headB;
        
        if(lengthA > lengthB) {
            
            currA = startListTraversalFromOffset(currA, lengthA - lengthB);
        }
        else {
            
            currB = startListTraversalFromOffset(currB, lengthB - lengthA);
        }
        
        while(currA != null) {
            
            if(currA == currB) {
                
                return currA;
            }
            
            currA = currA.next;
            currB = currB.next;
        }
        

        return null;
    }
    
    ListNode startListTraversalFromOffset(ListNode curr, int offSet) {
        
        while(offSet > 0) {
            
            curr = curr.next;
            offSet--;
        }
        
        return curr;
    }
    
    protected int getListLength(ListNode curr) {
        
        int length = 0;
        
        while(curr != null) {
            
            length++;
            curr = curr.next;
        }
        
        return length;
    }
}
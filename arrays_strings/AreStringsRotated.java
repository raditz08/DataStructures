
import java.lang.Math;
import java.util.*;

public class AreStringsRotated{

	public static void main(String[] args){

		AreStringsRotated sc = new AreStringsRotated();

		char []arr = {'a', 'b', 'c', 'd', 'e'};

		String str1 = "waterbottle",
			   str2 = "tlewaterbot";

		System.out.println(" are: " + sc.checkStringsRotated(str1,str2));

	}

	protected boolean checkStringsRotated(String str1, String str2) {

		if(str1.length() != str2.length()) {

			return false;
		}

		int index1 = 0,
			index2 = 0,
			startIndex = -1;
		boolean matchedPreviousChar = false;

		while(index2 < str2.length()) {

			if(str1.charAt(index1) != str2.charAt(index2)) {

				index1 = 0;
				startIndex = -1;
				matchedPreviousChar = false;				
			}
			else{

				System.out.println(" index2: " + index2 + " index1: " + index1);

				if(!matchedPreviousChar) {
					startIndex = index2;
					matchedPreviousChar = true;
				}
				
				index1++;
			}

			index2++;
		}

		if(startIndex == -1) {

			return false;
		}

		return str2.substring(0, startIndex).equals(str1.substring(str1.length() - startIndex));
	}
}


#include <iostream>
#include <string>
#include <vector>

using namespace std;

string encode(vector<string>& strs);
vector<string> decode(string s);

int main(void) {

    vector<string> input = {"", "def"};

    string res = encode(input);

    decode(res);
    
    return 0;
}



    // Encodes a list of strings to a single string.
    string encode(vector<string>& strs) {
        
        string res;
        
        if(strs.size() > 0) {
            string num_str = to_string(strs.size());
        
            res.insert(0, num_str);

            cout << " res: " << res << endl;


            res.push_back(' ');

            cout << " res: " << res << endl;
        
            for(auto str : strs) {
            
                res.insert(res.size(), to_string(str.size()));
                res.push_back(' ');

                cout << " res: " << res << endl;
            }
        
            for(auto str : strs) {
            
                res.insert(res.size(), str);
                //res.push_back(' ');
            }
        }
        
        cout << " res: " << res << endl;
        
        return res;
    }

    // Decodes a single string to a list of strings.
    vector<string> decode(string s) {
        
        vector<string> res;
        
        
        if(s.size() > 0) {
            
            string num_str;
            
            int index = 0;
            
            while(index < s.size() && s[index] != ' ') {

                cout << " s[index]: " << s[index] << endl;
                
                num_str.push_back(s[index]);
                ++index;
            }
            ++index;

            cout << " index: " << index << endl;
            
            int num = stoi(num_str);

            cout << " num: " << num << endl;
            
            vector<int> sizes(num);
            
            for(int strIndex = 0; strIndex < num; strIndex++) {
                
                string num_str;
                while(index < s.size() && s[index] != ' ') {

                    cout << " s[index]: " << s[index] << endl;
                
                    num_str.push_back(s[index]);
                    ++index;
                }

                cout << " index: " << index << endl;
                
                ++index;
                
                sizes[strIndex] = stoi(num_str);

                cout << " sizes[strIndex]: " << sizes[strIndex] << endl;
            }

            cout << " index: " << index << endl;
            

            for(int strIndex = 0; strIndex < num; strIndex++) {
                
                int len = sizes[strIndex];
                string curr;
                
                if(len > 0) {

                    cout << s[index] << endl;
                    
                    curr = s.substr(index, len);

                    cout << " curr: " << curr << endl;
                
                    index += sizes[strIndex];
                
                    
                }

                res.push_back(curr);
            }
        }

        for(auto res1 : res) {

            cout << " res: " << res1 << endl;
        }
        
        return res;
    }


import java.lang.Math;
import java.util.*;

public class StringCompression{

	public static void main(String[] args){

		StringCompression sc = new StringCompression();

		char []arr = {'a', 'b', 'c', 'd', 'e'};
		// sc.performCompressionInPlace(arr);

		String str = "aaaabbbbbccccdddede";

		System.out.println(" compString: " + sc.getCompressedString(str));
	}

	protected String getCompressedString(String str) {

		StringBuilder compString = new StringBuilder();

		int noOfChars = 0,
			compLength = 0;

		for(int ele=0; ele < str.length(); ele++) {

			noOfChars++;

			if(ele >= str.length()-1 || str.charAt(ele) != str.charAt(ele+1)) {

				compString.append(str.charAt(ele));
				compString.append(Integer.toString(noOfChars));
				compLength += Integer.toString(noOfChars).length() + 1;

				noOfChars = 0;
			}
		}

		System.out.println(" compLength: " + compLength);

		return compString.length() < str.length() ? compString.toString() : str;
	}

	protected void performCompressionInPlace(char [] arr) {
		
		int currentIndex = 0,
			newIndex = 0,
			noOfChars = 0;

		char currChar;
		String newString;

		while(currentIndex < arr.length) {

			currChar = arr[currentIndex];

			//New char sequence
			if(arr[currentIndex] != currChar) {

				newIndex++;
				if(newIndex < arr.length) {
					arr[newIndex] = '1';
				}
				else {
					return;
				}

				this.addCompIndex(arr, newIndex, noOfChars);
			}
			else {


			}
		}
	}

	protected void addCompIndex(char [] arr, int newIndex, int noOfChars) {

		String newString = Integer.toString(noOfChars);

		char [] nCharArray = newString.toCharArray();

		for(int ele=0; ele < nCharArray.length; ele++) {

			arr[ele + newIndex] = nCharArray[ele];
		}

	}
}

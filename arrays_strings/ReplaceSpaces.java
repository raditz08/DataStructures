
import java.util.*;
import java.text.*;

public class ReplaceSpaces {
	

	public static void main(String []args) {

		ReplaceSpaces isu = new ReplaceSpaces();

		char [] input = new char[9];
		input[0] = 'J';
		input[1] = ' ';
		input[2] = ' ';
		input[3] = 'O';
		input[4] = ' ';
		input[5] = 'H';

		isu.replaceSpaces(input, 9);
	}


	private void replaceSpaces(char []input, int size) {

		boolean finishedProcessing = false;
		int []shiftArray = new int[9];

		int shiftBy = 0;

		for(int ele=0; ele < 6 && !finishedProcessing; ele++) {

			if(input[ele] == ' ') {

				shiftArray[ele] = shiftBy;
				shiftBy += 1;
			}
			else {
				shiftArray[ele] = shiftBy;
			}
		}

		for(int ele=0; ele < shiftArray.length; ele++) {

			System.out.println(shiftArray[ele]);

		}

		for(int ele=5; ele > -1; ele--) {

			if(input[ele] == ' ') {

				input[ele + shiftArray[ele]] = '%';
				input[ele + shiftArray[ele] + 1] = 'd';
			}
			else {
				input[ele + shiftArray[ele]] = input[ele];
			}
		}



		System.out.println(" : " + Arrays.toString(input));
	}
}

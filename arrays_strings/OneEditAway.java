
import java.util.*;
import java.text.*;
import java.lang.Math.*;

public class OneEditAway {
	

	public static void main(String []args) {

		String str1 = args[0];
		String str2 = args[1];

		OneEditAway isu = new OneEditAway();

		boolean isStrUnique = isu.areOneEditAway(str1, str2);

		System.out.println(str1 + " " + str2 + ": " + isStrUnique);
	}


	private boolean areOneEditAway(String str1, String str2) {

		boolean foundDifferentChar = false;
		int noOfDiffChars = 0;

		if(Math.abs( str1.length() - str2.length()) > 1){

			return false;
		}


		String shorter = str1.length() < str2.length() ? str1 : str2;
		String longer = str1.length() < str2.length() ? str2 : str1;

		int shortIndex = 0,
			longIndex = 0;

		while(shortIndex < shorter.length() && longIndex < longer.length() && noOfDiffChars < 2) {

			if(shorter.charAt(shortIndex) == longer.charAt(longIndex)) {

				shortIndex++;
			}
			else {
				++noOfDiffChars;
				foundDifferentChar = true;
			}

			longIndex++;
		}

		return noOfDiffChars < 2;
	}
}

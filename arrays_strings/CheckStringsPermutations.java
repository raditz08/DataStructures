
import java.util.*;
import java.text.*;

public class AreStringsPermutations {
	

	public static void main(String []args) {

		String str1 = args[0];
		String str2 = args[1];

		AreStringsPermutations isu = new AreStringsPermutations();

		boolean isStrUnique = isu.arePermutations(str1, str2);

		System.out.println(str1 + str2 + ": " + isStrUnique);
	}


	private boolean arePermutations(String str1, String str2) {

		boolean differentCharFound = false;

		if(str1.length() != str2.length()){

			return false;
		}


		int []charOccurenceSet = new int[128];

		for(int s1=0; s1 < str1.length(); s1++) {

			charOccurenceSet[str1.charAt(s1)] +=1;
			charOccurenceSet[str2.charAt(s1)] -=1;
		}

		for(int s1=0; s1 < 128 && differentCharFound; s1++) {

			if(charOccurenceSet[s1] != 0) {

				differentCharFound = true;
			}
		}

		return !differentCharFound;
	}
}

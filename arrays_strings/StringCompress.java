
import java.util.*;
import java.text.*;
import java.lang.Math.*;

public class StringCompress {
	

	public static void main(String []args) {

		String str1 = args[0];

		StringCompress isu = new StringCompress();

		String isStrUnique = isu.compress(str1);

		System.out.println(str1 + ": " + isStrUnique);
	}


	private String compress(String str) {

		if(str.length() == 0) {

			return "";
		}

		StringBuilder compressed = new StringBuilder();

		int index = 1;
		int count = 1;

		int compressedLength = 0;

		Character previousChar = str.charAt(0);

		while(index < str.length()) {

			if(str.charAt(index) == previousChar) {

				++count;
			}
			else {

				compressedLength += String.valueOf(count).length() + 1;

				compressed.append(previousChar);
				compressed.append(count);

				previousChar = str.charAt(index);
				count = 1;
			}

			++index;
		}

		compressedLength += String.valueOf(count).length() + 1;

		compressed.append(previousChar);
		compressed.append(count);

		System.out.println(" compressedLength: " + compressedLength);

		return compressed.length() > str.length() ? str : compressed.toString();
	}
}


#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;

int price[11] = {0,1,5,8,9,10,17,17,20,24,30};

int topDownAuxCut(int n, int p[], vector<int>& r){

	cout << " n " << n << endl;
	if(r[n] > 0)
		return r[n];

	if(n==0)
		return 0;

	int maxForN = 0;

	for(int i = 0; i<n; i++){

		cout << " i " << i << endl;
		// cout << " maxForN " << maxForN << " p[n-i]" << p[n-i] << " r[i]" << r[i] << endl;

		maxForN = max(maxForN, topDownAuxCut(i, p, r) + price[n - i]);
	}

	r[n] = maxForN;

	cout << " r[n] " << r[n] << endl;

	return r[n];
}


int main(void)
{

	cout << " bottom up memoization " << endl;

	vector<int> stored(11,0);

	for(int i = 1; i<11; i++){

		cout << " i " << i << endl;

		int optForI = 0;
 
		for(int j = 0;j < i+1; j++){

			cout << " j " << j;
			cout << " optForI " << optForI << " price[i-j] " << price[i-j] 
			                               << " stored[j] "<< stored[j] << endl;

			optForI = max(optForI, price[i-j] + stored[j]);
		}
		stored[i] = optForI;
		cout << " stored for i " << stored[i] << endl;
	}

    vector<int> topDownAuxCutArray(11,0);
	cout << " " << topDownAuxCut(10, price, topDownAuxCutArray);


	return 0;
}


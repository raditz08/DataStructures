public class lc_stock_buy_sell {
    public int maxProfit(int[] prices) {
        
        int []MP = new int[prices.length];
        MP[0] = 0;
        
        for(int i=1;i<prices.length;i++){
            
            int profitOnI = 0;
            
            for(int j=0;j<i;j++){
                
                if(profitOnI < prices[i] - prices[j]){
                    
                    profitOnI = prices[i] - prices[j];
                }
            }
            
            MP[i] = MP[i-1] < profitOnI ? profitOnI : MP[i-1];
        }
        
        return MP[prices.length];
    }
}
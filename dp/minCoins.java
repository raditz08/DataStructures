
import java.util.*;

public class minCoins {
    public int coinChange(int[] coins, int amount) {
        
        return mininumCoins(coins, amount);
    }
    
private int mininumCoins(int []coins, int amount){
        
        if(coins.length == 0){
            
            return -1;
        }
        
        if(amount == 0){
            
            return 0;
        }
        
        int []minCoins = new int[amount+1];
        
        //Initialize
        for(int sum=1; sum < amount+1; sum++){
            
            minCoins[sum] = amount + 1;
        }
        
        //Populate the C(i) entries
        for(int coin=0; coin < coins.length && coins[coin] < amount+1; coin++){
            
            minCoins[coins[coin]] = 1;
        }
        
        minCoins[0] = 0;
        
        //Traverse through sum
        for(int sum=1; sum < amount+1 && (minCoins[sum] == amount + 1); sum++){
              
            int minCoinsForSum = Integer.MAX_VALUE;
            
            //Traverse through coins
            for(int coin=0; coin < coins.length && coins[coin] < sum+1; coin++){
                
                if(minCoins[sum-coins[coin]] != amount + 1){
                    
                    minCoins[sum] = Math.min(minCoins[sum], minCoins[sum-coins[coin]] + 1);
                }
            }
            
            // minCoins[sum] = minCoinsForSum;
        }
        
        return minCoins[amount] == amount + 1 ? -1 : minCoins[amount];
    };
}
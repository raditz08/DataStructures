
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// import org.apache.commons.lang.ArrayUtils;

class InvesmentStrategy
{
    public static void main(String args[])
    {

        int m = 3,
            n = 4,
            sum = 2,
            f1 = 5,
            f2 = 1;

        int [][] r = {{1, 2, 3, 4},
        	          {3, 4, 2, 1},
                      {9, 1, 12, 3},
                      {1, 15, 2, 4}};

        int []path = {3, 0, 0, 0};

        int [][] optimalStrategy = new int[m+1][n];
        

        int currentPos = 3;

        for(int j=1; j<m+1; j++){

        	int maxProfit = 0;

        	for(int i=0; i<n; i++){

        		// System.out.println(" i: " + i);
        		int cost = (i == path[j-1]) ? f2 : f1;

        		System.out.println(" cost: " + cost);

        		if(maxProfit < r[j-1][path[j-1]]*r[j][i]-cost){

        			maxProfit = r[j-1][path[j-1]]*r[j][i]-cost;
        			currentPos = i;

        			System.out.println(" currentPos: " + currentPos);
        			System.out.println( " i: " + i + " maxProfit: " + maxProfit);
        		}
        	}

        	r[j][currentPos] = maxProfit;
        	path[j] = currentPos;
        	System.out.println(" j: " + j + " path[j]: " + path[j]);
        }
    }
}



import java.lang.*;
import java.util.*;

class StringPermutations{
	
	public static void main(String []args){

		char[] s = new char[]{'0', '1', '2'};

		StringPermutations sp = new StringPermutations();
		// sp.printPermutations(s, 0, s.length-1);

		int[] num = new int[]{0, 1, 2, 3};

		sp.permute(num);


	}

	//O(N) = (N-1)swaps, each has another call to O(N-1) function
	//O(N) = (N-1)*O(N-1) -> O(N) = N^N
	private void printPermutations(char[] s, int startIndex, int endIndex){

		System.out.println(" startIndex: " + startIndex + " endIndex: " + endIndex);

		if(startIndex > endIndex){
			return;
		}

		if(startIndex == endIndex){

			System.out.println(" print: " + s[startIndex]);
		}

		System.out.println(" firstChar: " + s[startIndex]);
		printPermutations(s, startIndex+1, endIndex);

		for(int k = startIndex + 1; k < endIndex + 1; k++){

			System.out.println("swapping " + s[startIndex] + s[k] );
			swap(s, startIndex, k);
			printPermutations(s, startIndex+1, endIndex);

			System.out.println("back swapping " + s[k] + s[startIndex] );
			swap(s, k, startIndex);
		}
	}

	private void swap(char[] s, int leftIndex, int rightIndex){

		char c = s[leftIndex];
		s[leftIndex] = s[rightIndex];
		s[rightIndex] = c;
	}

	private void printArray(char[] s, int startIndex, int endIndex){
		
		for(int index = startIndex; index < endIndex+1; index++){

			System.out.println(s[index]);
		}
	}




	/* NEW SOLUTION */
	public List<List<Integer>> permute(int[] nums) {
	   List<List<Integer>> list = new ArrayList<>();
	   // Arrays.sort(nums); // not necessary
	   backtrack(list, new ArrayList<>(), nums);
	   return list;
	}

	private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums){

	   if(tempList.size() == nums.length){

	   	  System.out.println("max size ");
	      list.add(new ArrayList<>(tempList));
	   } else{
	      for(int i = 0; i < nums.length; i++){ 
	      	System.out.println("char: " + String.valueOf(nums[i]));
	         
	         if(tempList.contains(nums[i])) { //This is where you skip the 1st element and go onto the 2nd one as the 1st element of your list

	         	System.out.println("contains: " + String.valueOf(nums[i]));
	         	continue; // element already exists, skip
	         }
	         System.out.println("adding: " + String.valueOf(nums[i]));
	         tempList.add(nums[i]);
	         System.out.println("inside: " + String.valueOf(nums[i]));
	         	backtrack(list, tempList, nums);	//Moves over from i+1 to length
	         System.out.println("outside: " + String.valueOf(nums[i]));
	         // System.out.println("before clear: " + tempList + String.valueOf(nums[i]));
	         	tempList.remove(tempList.size() - 1);
	         System.out.println("after clear: " + tempList + String.valueOf(nums[i]));
	      }
	   }
	} 
}

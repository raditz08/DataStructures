
class NumberOfStepsPermutations{
	
	public static void main (String[] args){

	    int n = 3;
/*	    calculateNumberOfPermutations(n);
	}

    protected int calculateNumberOfPermutations(int n){
*/	
	    int []permutations = new int[n+1];

    	permutations[0] = 1;

    	for(int step = 0; step < n; step++){

            if(step+1 < n+1){
	            permutations[step+1] += permutations[step];
	        }

    	    if(step+2 < n+1){
	            permutations[step+2] += permutations[step];
	        }

    	    if(step+3 < n+1){
	            permutations[step+3] += permutations[step];
	        }
	    }

    	System.out.println( " steps: " + permutations[n]);
    }
}

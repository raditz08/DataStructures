
import java.lang.String;
import java.util.*;

class StringPermutationsStringInterface{
	
	public static void main(String []args){

		char[] s = new char[]{'0', '1', '2', '3'};

		StringPermutationsStringInterface sp = new StringPermutationsStringInterface();
		sp.printPermutations(s, 0, s.length-1);
	}

	public List<String> printPermutations(char[] s, int startIndex, int endIndex){

		List<String> newPermutations= new ArrayList<String>(),
		             smallerPermutations;

		System.out.println(" startIndex: " + startIndex + " endIndex: " + endIndex);

		if(startIndex > endIndex){
			return newPermutations;
		}

		if(startIndex == endIndex){

			System.out.println(" print: " + s[startIndex]);

			String oneChar = String.valueOf(s[startIndex]);
			newPermutations.add(oneChar);
		}

		System.out.println(" firstChar: " + s[startIndex]);

		smallerPermutations = printPermutations(s, startIndex+1, endIndex);
		for(String str : smallerPermutations){
			newPermutations.add(str);
		}

		for(int k = startIndex + 1; k < endIndex + 1; k++){

			System.out.println("swapping " + s[startIndex] + s[k] );
			swap(s, startIndex, k);
			smallerPermutations = printPermutations(s, startIndex+1, endIndex);
			for(String str : smallerPermutations){
				newPermutations.add(str);
			}

			System.out.println("back swapping " + s[k] + s[startIndex] );
			swap(s, k, startIndex);
		}

		System.out.println(" size: " + newPermutations.size() + " startIndex: " + startIndex + " endIndex: " + endIndex);

		return newPermutations;
	}

	public void swap(char[] s, int leftIndex, int rightIndex){

		char c = s[leftIndex];
		s[leftIndex] = s[rightIndex];
		s[rightIndex] = c;
	}

	public void printArray(char[] s, int startIndex, int endIndex){
		
		for(int index = startIndex; index < endIndex+1; index++){

			System.out.println(s[index]);
		}
	}
}

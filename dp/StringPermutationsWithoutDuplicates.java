
import java.lang.String;
import java.util.*;

class StringPermutationsWithoutDuplicates{
	
	public static void main(String []args){

		char[] s = new char[]{'1', '1', '2', '3'};

		StringPermutationsWithoutDuplicates sp = new StringPermutationsWithoutDuplicates();
		List<String> totalPermutations = sp.printPermutations(s, 0, s.length-1);

		for(String perm : totalPermutations){

			System.out.println(" " + perm);
		}
	}

	public List<String> printPermutations(char[] s, int startIndex, int endIndex){

		List<String> newPermutations= new ArrayList<String>(),
		             smallerPermutations;

		List<Character> startCharacters = new ArrayList<Character>();

		String oneChar;

		// System.out.println(" startIndex: " + startIndex + " endIndex: " + endIndex);

		if(startIndex > endIndex){
			return newPermutations;
		}

		if(startIndex == endIndex){

			// System.out.println(" print: " + s[startIndex]);

			oneChar = String.valueOf(s[startIndex]);
			newPermutations.add(oneChar);
		}

		// System.out.println(" firstChar: " + s[startIndex]);

		startCharacters.add(s[startIndex]);
		oneChar = String.valueOf(s[startIndex]);

		smallerPermutations = printPermutations(s, startIndex+1, endIndex);
		for(String str : smallerPermutations){
			newPermutations.add(oneChar + str);
		}

		for(int k = startIndex + 1; k < endIndex + 1; k++){

			//If entry not in the list
			if(!startCharacters.contains(s[k])){

				// System.out.println("swapping " + s[startIndex] + s[k]);
				swap(s, startIndex, k);

				oneChar = String.valueOf(s[startIndex]);

				smallerPermutations = printPermutations(s, startIndex+1, endIndex);
				for(String str : smallerPermutations){
					newPermutations.add(oneChar + str);
				}

				// System.out.println("back swapping " + s[k] + s[startIndex] );
				swap(s, k, startIndex);

				//Add entry to the list
				startCharacters.add(s[k]);
			}
			else{
				System.out.println(" part of the map: " + s[k]);
			}
		}

		System.out.println(" size: " + newPermutations.size() + " startIndex: " + startIndex + " endIndex: " + endIndex);

		return newPermutations;
	}

	public void swap(char[] s, int leftIndex, int rightIndex){

		char c = s[leftIndex];
		s[leftIndex] = s[rightIndex];
		s[rightIndex] = c;
	}

	public void printArray(char[] s, int startIndex, int endIndex){
		
		for(int index = startIndex; index < endIndex+1; index++){

			System.out.println(s[index]);
		}
	}
}






#include <iostream>
#include <fstream>
#include <vector>

using namespace std;


template <typename T>
void optimalBst(vector<T> p, vector<T> q, int n)
{
	cout << " n  " << n << endl;
	int e[n+1][n+1], root[n+2][n+2];
	double w[n+2][n+2];

	for(int i = 1; i<=n+1; i++)
	{
		e[i][i-1] = q[i-1];
		w[i][i-1] = q[i-1];
	}

	for(int l = 1; l<=n; l++)
	{
		cout << "l " << l << endl;

		for(int i = 1; i<=(n-l+1); i++)
		{
			cout << "i " << i << endl;
			int j = i+l-1;
			e[i][j] = 500;
			w[i][j] = w[i][j-1] + p[j] + q[j];

			for(int r = i; r<=j; r++)
			{
				double t = e[i][r-1] + e[r+1][j] + w[i][j];
				if(t<e[i][j])
				{
					e[i][j] = t;
					root[i][j] = r;
					cout <<  " j " << j << " e " << t << " r " << r << endl;
				}
			}
			
		}

	}

	cout << " base root " << root[1][n] << endl;
	return;
}


int main(void)
{
	double Parr[]  = {0, 0.15, 0.1, 0.05, 0.1, 0.2};
	std::vector<double> p(Parr, Parr+sizeof(Parr)/sizeof(double));

	double Qarr[]  = {0.05, 0.1, 0.05, 0.05, 0.05, 0.1};
	std::vector<double> q(Qarr, Qarr+sizeof(Qarr)/sizeof(double));

    optimalBst<double>(p, q, sizeof(Parr)/sizeof(double));
	return 0;
}


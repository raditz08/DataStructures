public class MinStack {

    /** initialize your data structure here. */
    public MinStack() {
        
        min = null;
    }
    
    public void push(int x) {
        
        if(top == null) {
            
            top = new Node(x, x);
        }
        
        else {
            
            int minForNewNode = Math.min(top.val, x); //top.val > x ? x : top.val;
            
            Node newTop = new Node(x, minForNewNode);
            newTop.next = top;
            top = newTop;
        }
    }
    
    public void pop() {
        
        if(top != null) {
            
            top = top.next;
        }
    }
    
    public int top() {
        
        if(top != null) {
            
            return top.val;
        }
        
        return 0;
    }
    
    public int getMin() {
        
        if(top != null) {
            
            return top.minVal;
        }
        
        return 0;
    }
    
    protected Node top;
    protected Node min;
    
    protected class Node{
        
        Node(int val, int minVal) {
            
            this.val = val;
            this.minVal = minVal;
            this.next = null;
        }
        
        protected int val;
        protected int minVal;
        protected Node next;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
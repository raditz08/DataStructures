
public class QueuesBasic{
	

	public static void main(String []args) {

		// Node start = new Node(4);

		QueuesBasic qb = new QueuesBasic();

		qb.initQueue();
		qb.printQueue();
	}

	protected Node front, back;

	protected void printQueue() {

		System.out.println(" non null back: " + back.val);

		Node curr = front;

		while(curr != null) {

			curr.printNode();
			curr = curr.next;
		}
	}

	protected void initQueue() {

		 back = addNode(front, back, 1);
		 back = addNode(front, back, 2);
		 back = addNode(front, back, 3);
		 back = addNode(front, back, 4);
		 back = addNode(front, back, 5);
		 back = addNode(front, back, 6);
		 back = addNode(front, back, 7);
		 back = addNode(front, back, 8);
		 back = addNode(front, back, 9);
	}

	protected Node addNode(Node front, Node back, int val) {

		if(back != null) {

			System.out.println(" non null");

			Node newNode = new Node(val);
			back.next = newNode;
			back = newNode;
		}
		else {

			back = new Node(val);
		}

		//IMPORTANT: THIS IS NOT BEING SET INSIDE THIS METHOD
		if(front == null) {

			front = back;
		}
		else {

			System.out.println(" non null front: " + front.val);
		}

		return back;
	}

	protected Node removeNode() {

		if(front == null) {

			//throw NoSuchElementException
		}

		Node t = new Node(front.val);

		front = front.next;

		if(back == null) {

			back = front;
		}

		return t;
	}
}



